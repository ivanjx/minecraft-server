FROM openjdk:18.0.2.1-slim-buster

# Download requirements.
WORKDIR /download
RUN apt-get update && \
    apt-get install -y wget curl && \
    wget https://piston-data.mojang.com/v1/objects/84194a2f286ef7c14ed7ce0090dba59902951553/server.jar

# Initialize minecraft server.
RUN java -Xmx768M -Xms256M -jar server.jar nogui && \
    sed -i 's/eula=false/eula=true/g' eula.txt && \
    sed -i 's/online-mode=true/online-mode=false/g' server.properties

# Add runner script.
WORKDIR /
COPY ./run.sh .
RUN chmod +x run.sh

# Done.
EXPOSE 25565
ENTRYPOINT [ "bash", "run.sh" ]
