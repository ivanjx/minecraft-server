if [ ! -f /data/eula.txt ]; then
    mkdir -p /data
    mv /download/* /data
fi

cd /data
exec java -Xmx1024M -Xms256M -jar server.jar nogui
